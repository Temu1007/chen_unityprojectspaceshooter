﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController2 : MonoBehaviour {

    public GameObject shot;
    public Transform shotSpawn;
    public Transform shotSpawn2;
    public Transform shotSpawn3;
    public float fireRate;
    public float delay;

    private AudioSource audioSource;

	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
        InvokeRepeating("Fire", delay, fireRate);
    }

    void Fire()
    {
        Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        audioSource.Play();
        Instantiate(shot, shotSpawn2.position, shotSpawn2.rotation);
        audioSource.Play();
        Instantiate(shot, shotSpawn3.position, shotSpawn3.rotation);
        audioSource.Play();
    }
}

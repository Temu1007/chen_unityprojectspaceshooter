﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Timers;


public class PlayerController : MonoBehaviour
{

    public float speed;
    public float tilt;
    public float xMin, xMax, zMin, zMax;
    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;
    private float nextFire;
    public GameObject playerExplosion;
    private GameController gameController;

    int health;
    int maxHealth = 5;
    public Slider healthBar;

    bool poweredUp;

    public Slider powerupBar;
    float powerLevel;

    float powerStart;

    private void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            GetComponent<AudioSource>().Play();
        }

        if (poweredUp)
        {
            if (Time.time >= powerStart + 5f)
            {
                fireRate = 0.25f;
                poweredUp = false;
            }
        }
        powerUp();
    }
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }

        health = maxHealth;
        UpdateHealthBar();

        
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.velocity = movement * speed;

        rb.position = new Vector3(
            Mathf.Clamp(rb.position.x, xMin, xMax),
            0.0f,
            Mathf.Clamp(rb.position.z, zMin, zMax)
        );

        rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -tilt);
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        UpdateHealthBar();

        if (health <= 0)
            Death();
    }

    public void powerUp()
    {
        if (poweredUp == false)
        {
            powerLevel += Time.deltaTime * 0.1f;

            if (Input.GetKey(KeyCode.Space) && powerLevel >= 1)
            {
                fireRate = 0.1f;
                powerLevel = 0f;
                poweredUp = true;
                powerStart = Time.time;
            }

            UpdatepowerBar();
        }
    }

    void UpdatepowerBar()
    {
        powerupBar.value = powerLevel;
    }

    void Death()
    {
        Instantiate(playerExplosion, transform.position, transform.rotation);
        gameController.GameOver();
        Destroy(gameObject);
    }

    void UpdateHealthBar()
    {
        healthBar.value = health / (float)(maxHealth);
    }

}